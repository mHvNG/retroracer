class Race {

    constructor(app) {
        this.app = app
        this.hit = false
        this.onready = 0
        this.lapCount = 0
        this.lapAmount = 3
        this.isOnce = true
        this.endTime = null
        this.timer = new Time()
        this.HUD = new HUD(this.app)
        this.parser = new Parser(null)
        this.popup = new PopUp(this.app)
        this.collision = new Collision()
        this.dhandler = new DataHandler()
        this.startTime = this.timer.timerStart()
        this.loader = new Map({
            app: this.app,
            collision: this.collision,
            map: Indianapolis
        })
        this.loader.drawAll()
        this.player = new Player(this.app)
        this.botHard = new StaticBotHard({
            app: this.app,
            map: Indianapolis
        })
    }

    set scoreDisplay({ content }) {
        // this.popup.color = 0xFCB0B3
        this.popup.properties = {
            x: 1100,
            y: 100,
            w: 400,
            h: 700
        }
        this.popup.text = {
            x: 50,
            y: 50,
            tSize: 32,
            tColor: 'black',
            content: content
        }
    }

    set score({ url }) {
        let xhr = new XMLHttpRequest()
        xhr.open('GET', url, true)
        xhr.send(null)
        xhr.onreadystatechange = () => {
            if (xhr.readyState == 4) {
                this.parser.times = { object: xhr.responseText }
                this.scoreDisplay = {
                    content: 'RetroRacer - Times'
                }
                this.popup.text = {
                    x: 75,
                    y: 150,
                    tSize: 20,
                    tColor: 'black',
                    content: 'Qualification - '
                }
                this.popup.text = {
                    x: 75,
                    y: 225,
                    tSize: 20,
                    tColor: 'black',
                    content: 'Race - '
                }
                this.popup.text = {
                    x: 250,
                    y: 150,
                    tSize: 20,
                    tColor: 'black',
                    content: this.parser.times[0]
                }
                this.popup.text = {
                    x: 250,
                    y: 225,
                    tSize: 20,
                    tColor: 'black',
                    content: this.parser.times[1]
                }
            }
        }
    }

    run() {
        this.player.mechanics()
        let timePassed = this.timer.elapsed({ type: timeType.MinSec, start: this.startTime })
        this.HUD.timer = timePassed
        this.HUD.lap = this.lapCount
        this.botHard.run()
        this.collisionTag = this.collision.onCollision({
            attributes: [this.player.position[0], this.player.position[1], this.player.position[2], this.player.position[3]],
            type: Collisions.Point
        })
        if (this.collisionTag === 'finishline' && !this.hit) {
            this.hit = true
            this.lapCount++
        } else if (this.collisionTag === undefined && this.hit) {
            this.hit = false
        }
        if (this.lapCount == this.lapAmount && this.onready < 10) { 
            this.onready++
            this.endTime = this.timer.elapsed({ type: timeType.MinSec, start: this.startTime })
            if (this.isOnce) {
                this.dhandler.upload({
                    key: 'race',
                    object: { 'time': { 'min': this.endTime[0], 'sec': this.endTime[1] } }
                })
                this.isOnce = false
            }
            this.score = {
                url: '/resources/score.php'
            }
            this.popup.draw()
        }
        if (this.onready == 10) {
            let timeBreak = performance.now() + 5000
            while (true) { 
                if (performance.now() > timeBreak) { break }
            }
            for (let i = this.app.stage.children.length - 1; i >= 0; i--) {
                this.app.stage.removeChild(this.app.stage.children[i])
            }
            return 'Race'
        }
    }
}