class HUD {

    constructor(app) {
        this.app = app
        this.text = null
        this.lapText = null
    }

    set timer(time) {
        for (let i = this.app.stage.children.length - 1; i >= 0; i--) {
            if (this.app.stage.children[i].name == 'timer') {
                this.app.stage.removeChild(this.app.stage.children[i])
            }
        }
        this.text = new PIXI.Text(time, {
            fontFamily: 'Arial',
            fontSize: 24,
            fill: 'white',
            align: 'center'
        })
        this.text.name = 'timer'
        this.text.x = 50
        this.text.y = 50
        this.app.stage.addChild(this.text)
    }

    set lap(lapCount) {
        for (let i = this.app.stage.children.length - 1; i >= 0; i--) {
            if (this.app.stage.children[i].name == 'lap') {
                this.app.stage.removeChild(this.app.stage.children[i])
            }
        }
        this.lapText = new PIXI.Text(lapCount, {
            fontFamily: 'Arial',
            fontSize: 32,
            fill: 'white',
            align: 'center'
        })
        this.lapText.name = 'lap'
        this.lapText.x = 1550
        this.lapText.y = 850
        this.app.stage.addChild(this.lapText)
    }
}