class Qualification {

    constructor(app) {
        this.app = app;
        this.hit = false
        this.lapCount = 0
        this.lapAmount = 2
        this.endTime = null
        this.HUD = new HUD(this.app)
        this.timer = new Time()
        this.collision = new Collision()
        this.dhandler = new DataHandler()
        this.startTime = this.timer.timerStart()
        this.loader = new Map({
            app: this.app,
            collision: this.collision,
            map: Indianapolis
        })
        this.loader.drawAll()
        this.player = new Player(this.app)
    }

    run() {
        this.player.mechanics()
        let timePassed = this.timer.elapsed({ type: timeType.SecMilli, start: this.startTime })
        this.HUD.timer = timePassed
        this.collisionTag = this.collision.onCollision({
            attributes: [this.player.position[0], this.player.position[1], this.player.position[2], this.player.position[3]],
            type: Collisions.Point
        })
        if (this.collisionTag === 'finishline' && !this.hit) {
            this.hit = true
            this.endTime = this.timer.elapsed({ type: timeType.SecMilli, start: this.startTime })
            this.lapCount++
        } else if (this.collisionTag === undefined && this.hit) {
            this.hit = false
        }
        if (this.lapCount == this.lapAmount) {
            this.dhandler.upload({
                key: 'qualification',
                object: { 'time': { 'sec': this.endTime[0], 'milli': this.endTime[1] } }
            })
            let timeBreak = performance.now() + 3000
            while (true) { 
                if (performance.now() > timeBreak) { break }
            }
            for (let i = this.app.stage.children.length - 1; i >= 0; i--) {
                this.app.stage.removeChild(this.app.stage.children[i])
            }
            return 'Qualification'
        }
    }

}
