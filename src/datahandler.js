/*
    This file contains the upload function.
    The uploaded data needs to be an object.
*/

class DataHandler {

    constructor() {

    }

    upload({ key, object }) {
        document.cookie = `${key}=${JSON.stringify(object)};`
    }

    retrieve({ key }) {
        this.name = `${key}=`
        this.ca = document.cookie.split(';')
        for (let cookie in ca) {
            c = ca[cookie]
            while (c.charAt(0) == '') {
                c = c.substring(1)
            }
            if (c.indexOf(this.name) == 0) {
                return c.substring(this.name.length, c.length)
            }
        }
        return null
    }

    clear() {
        let res = document.cookie
        let multiple = res.split(';')
        for (let items in multiple) {
            let key = multiple[items].split('=')
            switch (key[0]) {
                case ' qualification':
                    document.cookie = key[0] + " =; expires = Thu, 01 Jan 1970 00:00:00 UTC"
                    break;
                case 'race':
                    document.cookie = key[0] + " =; expires = Thu, 01 Jan 1970 00:00:00 UTC"
                    break;
            }
        }
    }
}