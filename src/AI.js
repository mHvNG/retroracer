class AI extends Car {

    constructor(app) {
        super({ 
            app: app, 
            x: 0, 
            y: 0, 
            w: 32, 
            h: 64 
        })
    }

    get currentPosition() {
        return super.position
    }

    set startPosition({ x, y, r }) {
        super.position = {
            x: x,
            y: y
        }
        super.rotation = { r: r }
    }

    accelerate() {
        super.acceleration('forward')
    }

    steer(degrees) {
        super.rotation = { r: degrees }
    }
}