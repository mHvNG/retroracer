const Indianapolis = {
    background: 'Map-Indianapolis',
    props: {
        paraplu: {
            position: [{
                x: 1000,
                y: 400
            }],
            scale: 2,
            angle: 0,
        }
    },
    interactive: {
        finishline: {
            position: [{
                x: 800,
                y: 12.5
            }],
            measurements: [{
                w: 32,
                h: 128
            }],
            scale: 1,
            angle: 0,
        }
    },
    checkpoints: {
        cp1: {
            x: [{
                x1: 1325,
                x2: 1325
            }],
            y: [{
                y1: 25,
                y2: 90
            }]
        },
        cp2: {
            x: [{
                x1: 1490,
                x2: 1550
            }],
            y: [{
                y1: 270,
                y2: 270
            }]
        },
        cp3: {
            x: [{
                x1: 1490,
                x2: 1550
            }],
            y: [{
                y1: 630,
                y2: 630
            }]
        },
        cp4: {
            x: [{
                x1: 1325,
                x2: 1325
            }],
            y: [{
                y1: 775,
                y2: 855
            }]
        },
        cp5: {
            x: [{
                x1: 270,
                x2: 270
            }],
            y: [{
                y1: 765,
                y2: 875
            }]
        },
        cp6: {
            x: [{
                x1: 25,
                x2: 90
            }],
            y: [{
                y1: 630,
                y2: 630
            }]
        },
        cp7: {
            x: [{
                x1: 25,
                x2: 90
            }],
            y: [{
                y1: 270,
                y2: 270
            }]
        },
        cp8: {
            x: [{
                x1: 270,
                x2: 270
            }],
            y: [{
                y1: 25,
                y2: 90
            }]
        },
    }
}