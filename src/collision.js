/*
    Collision class - mHvNG
    Included --> AABB collision - Circle 2 Circle collision
*/

const Collisions = {
    AABB: "AABB",
    Circle: "Circle",
    Point: "Point"
}

class Collision {

    constructor() {
        this.list = new Object()
        this.x, this.y, this.w, this.h
        this.pX, this.pY, this.pW, this.pH
    }

    get colliders() { return this.list }

    set collider({ tag, position, measurements, type }) {
        this.list[tag] = { position, measurements, type }
    }

    JSON2Array({ title, object }) {
        this.properties = [title]
        this.key = Object.values(object)
        Object.values(this.key).forEach((item, i) => {
            this.properties = [...this.properties, item]
        });
        return this.properties
    }

    point2AABB(attributes) {
        [this.x, this.y] = attributes
        this.register = this.colliders
        for (let items in this.register) {
            this.properties = this.JSON2Array({ title: items, object: this.register[items] })
            this.pX = this.properties[1][0]; this.pY = this.properties[1][1]
            this.pW = this.properties[2][0]; this.pH = this.properties[2][1]
            if ((this.x > this.pX && this.x < this.pX + this.pW) && (this.y > this.pY && this.y < this.pY + this.pH)) { return items }
        }
    }

    AABB(attributes) {
        [this.x, this.y, this.w, this.h] = attributes
        this.register = this.colliders
        for (let items in this.register) {
            this.properties = this.JSON2Array({ title: items, object: this.register[items] })
            this.pX = this.properties[1][0]; this.pY = this.properties[1][1]
            this.pW = this.properties[2][0]; this.pH = this.properties[2][1]
            if ((this.x < this.pX + this.pW) && (this.x + this.w > this.pX) && (this.y < this.pY + this.pH) && (this.y + this.h > this.pY)) { return items; }
        }
    }

    circle2circle(attributes) {
        [this.x, this.y, this.w, this.h] = attributes
        this.radius = (this.w / 2)
        this.register = this.colliders
        for (let items in this.register) {
            this.properties = this.JSON2Array({ title: items, object: this.register[items] })
            this.pos = this.properties[1]
            this.meas = this.properties[2]
            /* Calculate distances */
            this.dx = this.x - this.pos[0]
            this.dy = this.y - this.pos[1]
            this.radii = this.radius + (this.meas[0] / 2)
            if ((this.dx * this.dx + this.dy * this.dy) < (this.radii * this.radii)) { return items; }
        }
    }

    onCollision({ type, attributes }) {
        switch (type) {
            case Collisions.AABB:
                return this.AABB(attributes)
            case Collisions.Circle:
                return this.circle2circle(attributes)
            case Collisions.Point:
                return this.point2AABB(attributes)
            default:
                break;
        }
    }
}
