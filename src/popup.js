class PopUp {

    constructor(app) {
        this.app = app
        this.textList = new Array()
        this.x, this.y = null
        this.rect = PIXI.Sprite.from(PIXI.Texture.WHITE)
    }

    set color({ tint }) {
        this.rect.tint = tint
    }

    set properties({ x, y, w, h }) {
        x = x || undefined; y = y || undefined;
        if ((x && y) != undefined) {
            this.x = x; this.y = y;
            this.rect.x = x
            this.rect.y = y
        } 
        this.rect.width = w
        this.rect.height = h
    }

    set text({ x, y, tSize, tColor, content }) {
        let text = new PIXI.Text(content, {
            fontFamily: 'Arial',
            fontSize: tSize,
            fill: tColor,
            align: 'center'
        })
        text.x = x + this.x; text.y = y + this.y;
        this.textList = [...this.textList, text]
    }

    draw() {
        this.app.stage.addChild(this.rect)
        for (let items in this.textList) { this.app.stage.addChild(this.textList[items]) }
    }
}