var currentCode = new Array()

class Player extends Car {

    constructor(app) {
        super({ 
            app: app,
            x: 400,
            y: 75,
            w: 32,
            h: 64
        });
        super.rotation = {
            r: 90
        }
        this.keyCodes = [87, 65, 68]
    }

    registerKeyStrokes(e) {
        if (!'wda'.includes(e.key)) { return }
        this.keysDown = new Object()
        this.keysDown[e.key] = (event.type === 'keyup') ? 0 : 1
        Object.keys(this.keysDown).forEach((item, i) => {
            this.value = Object.values(this.keysDown)[i]
            switch (item) {
                case "w":
                    if (this.value && !currentCode.includes(87)) { currentCode.push(87); }
                    else if (!this.value && currentCode.includes(87)) { currentCode.splice(currentCode.indexOf(87)); }
                    break;
                case "a":
                    if (this.value && !currentCode.includes(65)) { currentCode.push(65) }
                    else if (!this.value && currentCode.includes(65)) { currentCode.splice(currentCode.indexOf(65)) }
                    break;
                case "d":
                    if (this.value && !currentCode.includes(68)) { currentCode.push(68) }
                    else if (!this.value && currentCode.includes(68)) { currentCode.splice(currentCode.indexOf(68)) }
                    break;
                default:
                    break;
            }
        });
    }

    mechanics() {
        document.addEventListener('keydown', this.registerKeyStrokes)
        document.addEventListener('keyup', this.registerKeyStrokes)
        if (currentCode.length) {
            if (currentCode.includes(this.keyCodes[0])) { this.acceleration("forward") }
            if (currentCode.includes(this.keyCodes[1]) && currentCode.includes(this.keyCodes[0])) { super.acceleration("steer", currentCode[1]) }
            if (currentCode.includes(this.keyCodes[2]) && currentCode.includes(this.keyCodes[0])) { super.acceleration("steer", currentCode[1]) }
        } else { super.deceleration(); }
    }
}