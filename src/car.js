class Car {

    constructor({ app, type, x, y, w, h }) {
        type = type || undefined;
        this.app = app
        this.type = type
        this.properties = [x, y, w, h]
        this._movingSpeed = 0
        this.multiplyer = 1
        this._maxSpeed = 6
        this._carSprite = PIXI.Sprite.from('../assets/sprites/Auto-1-1.png') 
        this.initCar()
    }

    initCar() {
        this._carSprite.anchor.set(0.5)
        this._carSprite.x = this.properties[0]
        this._carSprite.y = this.properties[1]
        this._carSprite.rotation = 0 * (Math.PI/180)
        this.app.stage.addChild(this._carSprite)
    }

    get position() {
        return [this._carSprite.x, this._carSprite.y, this._carSprite.width, this._carSprite.height];
    }

    get rotation() {
        return this._carSprite.rotation
    }

    set position({ x, y }) {
        this._carSprite.x = x
        this._carSprite.y = y
    }

    set rotation({ r }) {
        this._carSprite.rotation = r * (Math.PI/180)
    }

    acceleration(axis, keyCode) {
        keyCode = keyCode || undefined
        if (this._movingSpeed < this._maxSpeed) { this._movingSpeed += 0.001 * this.multiplyer; this.multiplyer++ }
        switch (axis) {
            case "forward":
                this._carSprite.position.x = this._carSprite.position.x + this._movingSpeed * Math.sin(this._carSprite.rotation)
                this._carSprite.position.y = this._carSprite.position.y - this._movingSpeed * Math.cos(this._carSprite.rotation) 
                break;
            case "steer":
                switch (keyCode) {
                    case 65:
                        this._carSprite.rotation = this._carSprite.rotation - 0.03
                        break;
                    case 68:
                        this._carSprite.rotation = this._carSprite.rotation + 0.03
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
    }

    deceleration() {
        if (this._movingSpeed > 0.1) {
            this.multiplyer = 0
            this._movingSpeed -= 0.3
            this._carSprite.position.x = this._carSprite.position.x + this._movingSpeed * Math.sin(this._carSprite.rotation)
            this._carSprite.position.y = this._carSprite.position.y - this._movingSpeed * Math.cos(this._carSprite.rotation)
        } 
    }
}
