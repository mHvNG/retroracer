/*
    Handling timer events with these two functions
*/

const timeType = {
    MinSec: 'MinSec',
    SecMilli: 'SecMilli'
}

class Time {

    constructor() {

    }

    timerStart() {
        return performance.now();
    }

    elapsed({ type, start }) {
        let timeDiff = performance.now() - start
        switch (type) {
            case timeType.MinSec:
                let minutes = 0
                timeDiff /= 1000
                let seconds = Math.round(timeDiff)
                while (seconds > 60) { seconds = seconds - 60; minutes++; }
                return [minutes, seconds]
            case timeType.SecMilli:
                let second = 0
                while (timeDiff > 1000) { timeDiff = timeDiff - 1000; second++; }
                return [second, timeDiff]
            default:
                return null
        }
    }
}
