var buttonClicked = null

class Start {

    constructor(app) {
        this.app = app
        this.dhandler = new DataHandler()
        this.dhandler.clear()
        this.app.renderer.backgroundColor = 0xFCB0B3
        this.buttonTitles = [
            'Start',
            'Leaderbord'
        ]
        this.button = new Button(this.app)
        this.button2 = new Button(this.app)
        this.button.name = this.buttonTitles[0]
        this.button2.name = this.buttonTitles[1]
        this.button.properties = {
            x: (this.app.screen.width / 2) - (150 / 2),
            y: 300,
            w: 150,
            h: 40
        }
        this.button2.properties = {
            x: (this.app.screen.width / 2) - (150 / 2),
            y: 375,
            w: 150,
            h: 40
        }
        this.button.text = {
            content: this.buttonTitles[0]
        }
        this.button2.text = {
            content: this.buttonTitles[1]
        }
        this.button.draw()
        this.button2.draw()
    }

    mousePos(event) {
        this.mpX = event.clientX - (window.innerWidth - 1600) / 2
        this.mpY = event.clientY - (window.innerHeight - 900) / 2
        for (let item in event.currentTarget.button) {
            if (event.currentTarget.button[item].onClick({ target: [this.mpX, this.mpY] }) == event.currentTarget.titles[item]) {
                buttonClicked = event.currentTarget.titles[item]
            }
        }
    }

    run(delta = null) {
        this.mouse = document.addEventListener('click', this.mousePos)
        document.button = [this.button, this.button2]
        document.titles = this.buttonTitles
        document.app = this.app
        switch(buttonClicked) {
            case this.buttonTitles[0]:
                for (let i = this.app.stage.children.length - 1; i >= 0; i--) {
                    this.app.stage.removeChild(this.app.stage.children[i])
                }
                document.removeEventListener('click', this.mousePos)
                this.app.renderer.backgroundColor = 0x000000
                buttonClicked = null
                return this.buttonTitles[0]
            case this.buttonTitles[1]:
                return this.buttonTitles[1]
            default:
                break
        }
    }
}