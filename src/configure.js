class Configure {

    constructor({ map, collision }) {
        this.parser = new Parser(map)
        this.collision = collision
        this.path = '../assets/sprites/'
    }  

    get background() {
        let background = PIXI.Sprite.from(this.path + this.parser.background)
        return background
    }

    get props() {
        let objects = new Array()
        for (let items in this.parser.props) {
            let item = this.parser.props[items]
            let sprite = PIXI.Sprite.from(this.path + item[0])
            sprite.x = item[1][0]; sprite.y = item[1][1];
            sprite.scale.set(item[2], item[2])
            sprite.rotation = item[3] * (Math.PI/180)
            objects.push(sprite)
        }
        return objects
    }

    get interactive() {
       let objects = new Array()
       for (let items in this.parser.interactive) {
            let item = this.parser.interactive[items]
            let sprite = PIXI.Sprite.from(this.path + item[0])
            sprite.x = item[1][0]; sprite.y = item[1][1];
            sprite.width = item[2][0]; sprite.height = item[2][1];
            sprite.rotation = item[4] * (Math.PI/180)
            this.collision.collider = {
                tag: item[0].split('.')[0],
                position: [sprite.x, sprite.y],
                measurements: [sprite.width, sprite.height],
                type: Collisions.AABB
           }
           objects.push(sprite)
       }
       return objects
    }
}