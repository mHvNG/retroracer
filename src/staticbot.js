class StaticBotHard extends AI {

    constructor({ app, map }) {
        super(app)
        this.map = map
        this.parser = new Parser(this.map)
        super.startPosition = {
            x: 400,
            y: 75,
            r: 90
        }
        this.pathIndex = 0
        this.start = true
        this.leading = new Array()
        this.path = this.racingline
        this.angles = this.steeringAngles
    }

    get racingline() {
        let path = new Array()
        let checkpoints = this.parser.checkpoints
        let isStart = true
        let isSameAxis = 0
        for (let items in checkpoints) {
            let coordinates = new Array()
            if (isStart) { path.push([super.currentPosition[0], super.currentPosition[1]]); isStart = false; }
            if (items == 0) { 
                isSameAxis = 1
                this.leading = [...this.leading, 1]
                let yCoord = Math.floor((Math.random() * (checkpoints[items][1][1] - checkpoints[items][1][0])) + checkpoints[items][1][0])
                let coords = [checkpoints[items][0][0], yCoord]
                coordinates = coords
            } else {
                if (isSameAxis == 1 && checkpoints[items][1][0] != checkpoints[items][1][1]) {
                    this.leading = [...this.leading, 1]
                    let coords = [checkpoints[items][0][0], path[items][1]]
                    coordinates = coords
                } else if (isSameAxis == 0 && checkpoints[items][0][0] != checkpoints[items][0][1]) {
                    this.leading = [...this.leading, 0]
                    let coords = [path[items][0], checkpoints[items][1][0]]
                    coordinates = coords
                } else if (checkpoints[items][1][0] != checkpoints[items][1][1]) {
                    /* pick random y position */
                    isSameAxis = 1
                    this.leading = [...this.leading, 1]
                    let yCoord = Math.floor((Math.random() * (checkpoints[items][1][1] - checkpoints[items][1][0])) + checkpoints[items][1][0])
                    let coords = [checkpoints[items][0][0], yCoord]
                    coordinates = coords
                } else if (checkpoints[items][0][0] != checkpoints[items][0][1]) {
                    isSameAxis = 0
                    this.leading = [...this.leading, 0]
                    /* pick random x position */
                    let xCoord = Math.floor((Math.random() * (checkpoints[items][0][1] - checkpoints[items][0][0])) + checkpoints[items][0][0])
                    let coords = [xCoord, checkpoints[items][1][0]]
                    coordinates = coords
                }
            }
            path.push(coordinates)
        }
        return path
    }

    get steeringAngles() {
        let angles = new Array()
        for (let items in this.path) {
            let index = parseInt(items) + 1
            if (this.path[index] === undefined) { 
                let dx = this.path[1][0] - this.path[items][0]
                let dy = this.path[1][1] - this.path[items][1]
                let theta = Math.atan2(dy, dx)
                theta *= 180/Math.PI
                theta = theta + 90
                angles.push(theta)
            } else {
                let dx = this.path[index][0] - this.path[items][0]
                let dy = this.path[index][1] - this.path[items][1]
                let theta = Math.atan2(dy, dx)
                theta *= 180/Math.PI
                if (angles[parseInt(items) -1] !== undefined) { 
                    theta = theta + 90
                }
                else { theta = theta + 90 }
                angles.push(theta)
            }
        }
        return angles
    }

    drivePath() {
        if (this.path[this.pathIndex +1] === undefined) { this.pathIndex = 0; this.start = true; }
        if (this.start) { super.steer(this.angles[this.pathIndex]); this.start = false }
        else if (parseInt(this.leading[this.pathIndex]) == 0 && super.currentPosition[0] >= (this.path[this.pathIndex +1][0] - 30) && super.currentPosition[0] <= (this.path[this.pathIndex +1][0] + 30)) {
            if (super.currentPosition[1] >= (this.path[this.pathIndex +1][1] - 5) && super.currentPosition[1] <= (this.path[this.pathIndex +1][1] + 5)) {
                super.steer(this.angles[this.pathIndex +1])
                this.pathIndex++
            }
        } else if (parseInt(this.leading[this.pathIndex]) == 1 && super.currentPosition[1] >= (this.path[this.pathIndex +1][1] - 30) && super.currentPosition[1] <= (this.path[this.pathIndex +1][1] + 30)) {
            if (super.currentPosition[0] >= (this.path[this.pathIndex +1][0] - 5) && super.currentPosition[0] <= (this.path[this.pathIndex +1][0] + 5)) {
                super.steer(this.angles[this.pathIndex +1])
                this.pathIndex++
            }
        }
    }

    run() {
        super.accelerate()
        this.drivePath()
    }
}

class StaticBotEasy extends AI {

    constructor({ app, map }) {
        super(app)
        this.map = map
        this.parser = new Parser(this.map)
        super.startPosition = {
            x: 400,
            y: 75,
            r: 90
        }
        this.pathIndex = 0
        this.start = true
        this.leading = new Array()
        this.path = this.racingline
        this.angles = this.steeringAngles
    }

    get racingline() {
        let path = new Array()
        let checkpoints = this.parser.checkpoints
        let isStart = true
        for (let items in checkpoints) {
            let coordinates = new Array()
            if (isStart) { path.push([super.currentPosition[0], super.currentPosition[1]]); isStart = false; }
            if (items == 0) { 
                this.leading = [...this.leading, 1]
                let yCoord = Math.floor((Math.random() * (checkpoints[items][1][1] - checkpoints[items][1][0])) + checkpoints[items][1][0])
                let coords = [checkpoints[items][0][0], yCoord]
                coordinates = coords
            } else {
                if (checkpoints[items][1][0] != checkpoints[items][1][1]) {
                    /* pick random y position */
                    this.leading = [...this.leading, 1]
                    let yCoord = Math.floor((Math.random() * (checkpoints[items][1][1] - checkpoints[items][1][0])) + checkpoints[items][1][0])
                    let coords = [checkpoints[items][0][0], yCoord]
                    coordinates = coords
                } else if (checkpoints[items][0][0] != checkpoints[items][0][1]) {
                    this.leading = [...this.leading, 0]
                    /* pick random x position */
                    let xCoord = Math.floor((Math.random() * (checkpoints[items][0][1] - checkpoints[items][0][0])) + checkpoints[items][0][0])
                    let coords = [xCoord, checkpoints[items][1][0]]
                    coordinates = coords
                }
            }
            path.push(coordinates)
        }
        return path
    }

    get steeringAngles() {
        let angles = new Array()
        for (let items in this.path) {
            let index = parseInt(items) + 1
            if (this.path[index] === undefined) { 
                let dx = this.path[1][0] - this.path[items][0]
                let dy = this.path[1][1] - this.path[items][1]
                let theta = Math.atan2(dy, dx)
                theta *= 180/Math.PI
                theta = theta + 90
                angles.push(theta)
            } else {
                let dx = this.path[index][0] - this.path[items][0]
                let dy = this.path[index][1] - this.path[items][1]
                let theta = Math.atan2(dy, dx)
                theta *= 180/Math.PI
                if (angles[parseInt(items) -1] !== undefined) { 
                    theta = theta + 90
                }
                else { theta = theta + 90 }
                angles.push(theta)
            }
        }
        return angles
    }

    drivePath() {
        if (this.path[this.pathIndex +1] === undefined) { this.pathIndex = 0; this.start = true; }
        if (this.start) { super.steer(this.angles[this.pathIndex]); this.start = false }
        else if (parseInt(this.leading[this.pathIndex]) == 0 && super.currentPosition[0] >= (this.path[this.pathIndex +1][0] - 30) && super.currentPosition[0] <= (this.path[this.pathIndex +1][0] + 30)) {
            if (super.currentPosition[1] >= (this.path[this.pathIndex +1][1] - 5) && super.currentPosition[1] <= (this.path[this.pathIndex +1][1] + 5)) {
                super.steer(this.angles[this.pathIndex +1])
                this.pathIndex++
            }
        } else if (parseInt(this.leading[this.pathIndex]) == 1 && super.currentPosition[1] >= (this.path[this.pathIndex +1][1] - 30) && super.currentPosition[1] <= (this.path[this.pathIndex +1][1] + 30)) {
            if (super.currentPosition[0] >= (this.path[this.pathIndex +1][0] - 5) && super.currentPosition[0] <= (this.path[this.pathIndex +1][0] + 5)) {
                super.steer(this.angles[this.pathIndex +1])
                this.pathIndex++
            }
        }
    }

    run() {
        super.accelerate()
        this.drivePath()
    }
}