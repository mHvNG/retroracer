class Parser {

    constructor(map) {
        this.map = map
        this.timesObject = null
    }

    set times({ object }) {
        this.timesObject = object
    }

    get times() {
        let lapTimesQuali = new Array(); let lapTimesRace = new Array();
        const json = JSON.parse(this.timesObject)
        for (let items in json.qualification) { lapTimesQuali = [...lapTimesQuali, json.qualification[items]] }
        for (let items in json.race) { lapTimesRace = [...lapTimesRace, json.race[items]] }
        let qualiTime = `${lapTimesQuali[0]}:${lapTimesQuali[1]}`
        let raceTime = `${lapTimesRace[0]}:${lapTimesRace[1]}`
        const list = [qualiTime, raceTime]
        return list
    }

    get background() {
        const sprite = (this.map.background + '.png')
        return sprite
    }

    get props() {
        let content = new Array()
        for (let items in this.map.props) {
            let item = new Array()
            item.push( (items + '.png') )
            let objects = this.map.props[items]
            for (let properties in objects) { 
                if (typeof(objects[properties]) === 'object') {
                    let coordinates = new Array() 
                    for (let coords in objects[properties][0]) { coordinates.push(objects[properties][0][coords]) }
                    item.push(coordinates)
                } else { item.push(objects[properties]) }
            }
            content.push(item) 
        }
        return content
    }

    get interactive() {
        let content = new Array()
        for (let items in this.map.interactive) {
            let item = new Array()
            item.push( (items + '.png') )
            let objects = this.map.interactive[items]
            for (let properties in objects) {
                if (typeof(objects[properties]) === 'object') {
                    let coordinates = new Array()
                    for (let coords in objects[properties][0]) { coordinates.push(objects[properties][0][coords]) }
                    item.push(coordinates)
                } else { item.push(objects[properties]) }
            }
            content.push(item)
        }
        return content
    }

    get checkpoints() {
        let content = new Array()
        for (let items in this.map.checkpoints) {
            let objects = this.map.checkpoints[items]
            let cpoints = new Array()
            for (let properties in objects) {
                let coordinates = new Array()
                for (let coords in objects[properties][0]) { coordinates.push(objects[properties][0][coords]) }
                cpoints.push(coordinates)
            }
            content.push(cpoints)
        }
        return content
    }
}