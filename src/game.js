const app = new PIXI.Application({
    width: document.getElementById('racer').offsetWidth,
    height: document.getElementById('racer').offsetHeight,
    antialias: true
})

document.getElementById('racer').appendChild(app.view)

var scene = null

function init() {
    if (this.scene == null) { this.scene = new Start(app) }
}

/* Initialise objects */
this.init()
/* Updates objects every frame */
app.ticker.add((delta) => {
    switch(this.scene.run()) {
        case 'Start':
            this.scene = new Qualification(app)
            break;
        case 'Leaderbord':
            window.location.href = '/resources/leaderbord.php'
            break;
        case 'Qualification':
            this.scene = new Race(app)
            break;
        case 'Race':
            this.scene = new Start(app)
            break;
    }
})
