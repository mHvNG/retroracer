class Map {

    constructor({ app, collision, map }) {
        this.app = app
        this.map = map
        this.configure = new Configure({
            map: this.map,
            collision: collision
        })
    }

    drawAll() {
        /* Draw background */
        this.app.stage.addChild(this.configure.background)
        /* Draw all props */
        for (let items in this.configure.props) { this.app.stage.addChild(this.configure.props[items]) }
        /* Draw all interactive props */
        for (let items in this.configure.interactive) { this.app.stage.addChild(this.configure.interactive[items]) }
    }

    draw(type) {
        switch (type) {
            case 'background':
                this.app.stage.addChild(this.configure.background)
                break;
            case 'props':
                for (let items in this.configure.props) { this.app.stage.addChild(this.configure.props[items]) }
                break;
            case 'interactive':
                for (let items in this.configure.interactive) { this.app.stage.addChild(this.configure.interactive[items]) }
                break;
        }
    }
}