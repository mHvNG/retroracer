class Button {

    constructor(app) {
        this.app = app
        this.pText = null
        this.buttonName = null
        this.rect = PIXI.Sprite.from(PIXI.Texture.WHITE)
    }

    set name(title) {
        this.buttonName = title
    }

    set color({ tint }) {
        this.rect.tint = tint
    }

    set properties({ x, y, w, h }) {
        x = x || undefined; y = y || undefined;
        if ((x && y) != undefined) {
            this.rect.x = x
            this.rect.y = y
        } 
        this.rect.width = w
        this.rect.height = h
    }

    set text({ content }) {
        this.pText = new PIXI.Text(content, {
            fontFamily: 'Arial',
            fontSize: 24,
            fill: 'black',
            align: 'center'
        })
        this.pText.x = this.rect.x + (this.rect.width / 2) - (this.pText.width / 2)
        this.pText.y = this.rect.y + (this.rect.height / 2) - (this.pText.height / 2)
    }

    draw() {
        this.app.stage.addChild(this.rect)
        this.app.stage.addChild(this.pText)
    }   

    onClick({ target }) {
        if (target[0] >= this.rect.x && target[0] <= (this.rect.x + this.rect.width)) {
            if (target[1] >= this.rect.y && target[1] <= (this.rect.y + this.rect.height)) { return this.buttonName }
        }
        return null
    }
}