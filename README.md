# README #

This README document contains information regarding this project.

* Waar kan ik de game vinden:  [RetroRacer](http://highscore.mathijshoving.nl).

| **Toetsen**     | **Acties**    |
| --------------- | ------------- |
| W - Arrow up    | Accelereren.  |
| A - Arrow left  | Sturen links. |
| D - arrow right | Sturen rechts |

**Gameplay walkthrough**

De speler start in een start menu waarin drie verschillende buttons (knoppen) staan: 

1. *Start*

2. *Leaderbords*

**Start** - Druk op start en dan zal de qualificatie beginnen. Als de qualificatie afgelopen is zal de speler automatisch na vijf seconden naar de race gaan. Als de speler de race klaar heeft, zal er voor tien seconden een popup staan met de tijden van de qualificatie & race en hierna zal de speler naar start worden gestuurd.

**Leaderbords** - Hier kan de speler de tijden zien van iedereen over de hele wereld. 

