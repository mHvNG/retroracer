<?php

include_once 'resources/src/datahandler.php';
include_once 'resources/src/generator.php';
include_once 'resources/src/database.php';
include_once 'resources/src/IO.php';

class IndexPage {

    private $handler;
    private $gen;
    private $dbase;
    private $io;

    function __construct() {
        $this->handler = new Datahandler();
        $this->gen = new Generate();
        $this->dbase = new Database();
        $this->io = new ReadWrite();
    }

    public function state() {
        session_start();
        if (!isset($_SESSION["visits"])) { $_SESSION["visits"] = 0; }
        // Set session visits +1.
        $_SESSION["visits"] = $_SESSION["visits"] + 1;
        if ($_SESSION["visits"] > 1 && $_SERVER['REQUEST_METHOD'] != 'POST') { unset($_POST); }
    }

    public function existence() {
        if (isset($_POST)) {
            $alertMessage = '';
            switch(true) {
                case $this->dbase->exists('Email', $_POST['email']) && $this->dbase->exists('Username', $_POST['username']):
                    $alertMessage = 'Given email address & username already in use... Try again!';
                    echo "<script type='text/javascript'>
                        alert('$alertMessage');
                    </script>";
                    break;
                case $this->dbase->exists('Email', $_POST['email']):
                    $alertMessage = 'Given email address already in use... Try again!';
                    echo "<script type='text/javascript'>
                        alert('$alertMessage');
                    </script>";
                    break;
                case $this->dbase->exists('Username', $_POST['username']):
                    $alertMessage = 'Given username already in use... Try again!';
                    echo "<script type='text/javascript'>
                        alert('$alertMessage');
                    </script>";
                    break;
                default:
                    $this->gen->verify('register', $_POST['email'], $_POST['username']);
                    $this->handler->upload('email', $_POST['email']);
                    header('Location: resources/verify.php');
                    break;
            }
        }
    }

    public function render() {
        echo $this->io->read('resources/templates/index-content.html');
    }
}

$indexPage = new IndexPage();

/* --> */ $indexPage->state();
/* --> */ $indexPage->existence();
/* --> */ $indexPage->render();

?>