CREATE TABLE users (
    Email varchar(255),
    Username varchar(255),
    VerificationCode varchar(255),
    QTime varchar(255),
    RTime varchar(255),
    Continent varchar(255)
);

INSERT INTO users (Email, Username, VerificationCode, QTime, RTime, Continent) VALUES ('mathijs@hdms.nl', 'KeizerMathijs', '1234567iuy', NULL, NULL, NULL);
