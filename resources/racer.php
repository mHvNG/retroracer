<?php

class Racer {

    private $scripts;

    function __construct() {
        /* Make sure the order is correct! */
        $this->scripts = [
            '../extern/pixi.min.js',
            '../src/collision.js',
            '../src/timer.js',
            '../src/datahandler.js',
            '../src/car.js',
            '../src/player.js',
            '../src/AI.js',
            '../src/staticbot.js',
            '../src/button.js',
            '../src/popup.js',
            '../src/HUD.js',
            '../src/maps.js',
            '../src/parser.js',
            '../src/configure.js',
            '../src/maploader.js',
            '../src/start.js',
            '../src/qualification.js',
            '../src/race.js',
            '../src/game.js'
        ];
    }

    public function render() {
        $firstBody = '
            <html>
                <head>
                    <title>RetroRacer</title>
                </head>
                <style>
                    #racer {
                        width: 1600px;
                        height: 900px;
                        display: block;
                        margin: auto;
                    }
                </style>
                <body>
                    <div id="racer" style=" width: 1600px; height: 900px; ">
        ';
        foreach($this->scripts as $script) { $firstBody.= '<script type="text/javascript" src="'. $script. '"></script>'; }
        $firstBody .= '
                    </div>
                </body>
            </html>
        ';
        echo $firstBody;
    }
}

$racer = new Racer();

/* --> */ $racer->render();

?>
