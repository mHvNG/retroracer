<?php

include_once 'src/generator.php';
include_once 'src/database.php';
include_once 'src/connection.php';
include_once 'src/IO.php';

class Score {

    private $gen;

    function __construct() {
        $this->gen = new Generate();
    }

    private function getTimes() {
        $scoreQuali = NULL;
        $scoreRace = NULL;
        switch (true) {
            case isset($_COOKIE['qualification']):
                $scoreQuali = json_decode($_COOKIE['qualification']);
            case isset($_COOKIE['race']):
                $scoreRace = json_decode($_COOKIE['race']);
        }
        $scoreQualiSec = $scoreQuali->{'time'}->{'sec'};
        $scoreQualiMilli = $scoreQuali->{'time'}->{'milli'};
        $scoreRaceMin = $scoreRace->{'time'}->{'min'};
        $scoreRaceSec = $scoreRace->{'time'}->{'sec'};
        return [$scoreQualiSec, $scoreQualiMilli, $scoreRaceMin, $scoreRaceSec];
    }

    private function setScoreString() {
        $qualiTime = $this->getTimes()[0]. ':'. $this->getTimes()[1];
        $raceTime = $this->getTimes()[2]. ':'. $this->getTimes()[3];
        return [$qualiTime, $raceTime];
    }

    private function ifSet() {
        if (isset($_COOKIE['qualification']) && isset($_COOKIE['race'])) {
            return true;
        }
    }

    public function sendData() {
        $username = $_COOKIE['username'];
        if ($this->ifSet() == true) {
            $this->gen->setTimesDatabase($username, $this->setScoreString()[0], $this->setScoreString()[1]);
        }
    }

    public function displayScore() {
        $objToShow = '{"qualification": {"sec": '. $this->getTimes()[0]. ', "milli": '. $this->getTimes()[1]. '}, "race": {"min": '. $this->getTimes()[2]. ', "sec": '. $this->getTimes()[3]. '}}';
        echo $objToShow;
    }
}

$score = new Score();
$score->displayScore();
$score->sendData();

?>