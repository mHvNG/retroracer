<?php

header('Content-type: text/css');

$fontFamily = 'PressStart2PRegular';
$fontFamilyForm = 'Open Sans';

?>

@import url(https://fontlibrary.org/face/press-start-2p);
@import url(https://fonts.googleapis.com/css2?family=Open+Sans&display=swap);

body {
    background-color: #FCB0B3;
}

h1 {
    font-family: <?=$fontFamily?>
}

h2 {
    font-family: <?=$fontFamilyForm?>
}

p {
    font-family: <?=$fontFamilyForm?>;
}

#RegisterVerification {
    background-color: #BAD7F2;
    width: 800px;
    height: 800px;
    position: absolute;
    top: 50%;
    left: 50%;
    margin-right: -50%;
    transform: translate(-50%, -50%);
    border: 6px solid #235789;
    border-radius: 15px;
}

#Title {
    margin-top: 10%;
    text-align: center;
    font-size: 32px;
    color: #2374AB;
}

#Subtitle {
    margin-top: 5%;
    text-align: center;
    font-size: 24px;
    color: #2374AB;
}

#steps {
    display: block;
    margin: auto;
    margin-top: 5%;
    width: 70%;
    text-align: center;
    font-style: italic;
}

#steps p {
    color: #2374AB;
}

#verificationForm {
    margin-top: 7%;
    margin-left: 20%;
    margin-right: 20%;
    width 60%;
}

input {
    width: 100%;
    height: 40px;
    border: 3px solid #FCB0B3;
    border-radius: 10px;
    font-size: 16px;
}

input[type=text] {
    padding-left: 40px;
}

input[type=text]:focus {
    outline: none;
    background-color: #FCB0B3;
}

input[type=submit] {
    background-color: #FCB0B3;
    margin-top: 1%;
    border: 0px;
    border-radius: 7px;
    color: white;
    cursor: pointer;
}

#code-input {
    background-image: url('../../assets/icons/Verification.png');
    background-position: 10px 10px;
    background-repeat: no-repeat;
}

#subject {
    font-family: <?=$fontFamilyForm?>, sans-serif;
    font-weight: bold;
}