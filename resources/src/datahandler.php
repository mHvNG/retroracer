<?php

class Datahandler {

    function __construct() {

    }

    public function upload($key, $value) {
        setcookie($key, $value);
    }

    public function retrieve($key) {
        if (!isset($_COOKIE[$key])) {
            return null;
        } else return $_COOKIE[$key];
    }
}

?>