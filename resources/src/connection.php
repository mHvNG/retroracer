<?php

class Connection {

    private $io;
    private $content;

    function __construct() {
        $this->io = new ReadWrite();
        error_reporting(E_ALL ^ E_WARNING);
        // Two options depends on the current folder.
        if ($this->io->read('resources/ignored/dbase.json') == null) { $this->content = $this->io->read('ignored/dbase.json'); }
        else $this->content = $this->io->read('resources/ignored/dbase.json');
        $this->content = $this->io->parse($this->content, 'c37792RetroRacerUsers');
    }

    public function connectTo() {
        $HOST = $this->content[0];
        $username = $this->content[1];
        $password = $this->content[2];
        $databaseName = $this->content[3];
        // Create a connection with the database
        $conn = mysqli_connect($HOST, $username, $password, $databaseName);
        if (mysqli_connect_errno()) { die("Connection failed - ". mysqli_connect_error()); } // Passed the error check
        return $conn;
    }
}

?>