<?php 

class Generate {

    private $chars;
    private $dbase;
    private $conn;

    function __construct() {
        error_reporting(0);
        $this->chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789';
        $this->dbase = new Database();
        $this->conn = new Connection();
    }

    private function VerificationCode() {
        $code = '';
        $seed = str_split($this->chars);
        shuffle($seed);
        foreach(array_rand($seed, 10) as $k) $code.= $seed[$k];
        return $code;
    }

    private function emailContent($username, $verifyCode) {
        return "
            <html>
                <head>
            
                </head>
                <style>
                    @import url(https://fontlibrary.org/face/press-start-2p);
                    @import url(https://fonts.googleapis.com/css2?family=Open+Sans&display=swap);
            
                    body {
                        background-color: #FCB0B3;
                    }
            
                    h1 {
                        font-family: 'PressStart2PRegular';
                        color: #2374AB;
                    }
            
                    h2, h4 {
                        font-family: 'open Sans', sans-serif;
                        color: #2374AB;
                    }
            
                    h4 {
                        font-size: 15px;
                    }
            
                    p {
                        font-family: 'open Sans', sans-serif;
                    }
            
                    #email-content {
                        background-color: #BAD7F2;
                        width: 40%;
                        height: 40%;
                        position: absolute;
                        top: 50%;
                        left: 50%;
                        margin-right: -50%;
                        transform: translate(-50%, -50%);
                        border: 6px solid #235789;
                        border-radius: 15px;
                    }
            
                    #Title {
                        text-align: center;
                        padding-top: 5%;
                    }
            
                    #SubTitle {
                        text-align: center;
                    }
            
                    #verifyCode {
                        display: block;
                        margin: auto;
                        width: 80%;
                        margin-top: 5%;
                    }
            
                    #verifyCode p {
                        font-weight: bold;
                        font-size: 16px;
                        text-align: center;
                    }
            
                    #aboutGame {
                        display: block;
                        margin: auto;
                        width: 80%;
                        margin-top: 5%;
                    }
            
                    #aboutGame p {
                        font-size: 12px;
                        font-style: italic;
                        text-align: center;
                        color: #2374AB;
                    }
                </style>
                <body>
                    <div id='email-content'>
                        <h1 id='Title'>RetroRacer</h1><br>
                        <h2 id='SubTitle'>Welcome ${username}!</h2>
                        <h4 id='SubTitle'>To verify your account paste the verification code into the field.</h4>
                        <div id='verifyCode'>
                            <p>Verification code: ${verifyCode}</p>
                        </div>
                        <div id='aboutGame'>
                            <p>RetroRacer is a racing game with a retro feel to it. RetroRacer is about setting lap records. The lap records can be found in the global leaderbords.</p>
                        </div>
                    </section>
                </body>
            </html>
        ";
    }

    private function send($email, $subject, $message) {
        $headers = 'From: mail@mathijshoving.nl'. "\r\n". 
            "Reply-To: ${$email}". "\r\n". 
            "Content-type: text/html; charset=iso-8859-1;". "\r\n";
        $mail = mail($email, $subject, $message, $headers);
    }

    private function toDatabase($email, $username, $verifyCode) {
        $sql = "
            INSERT INTO users (Email, Username, VerificationCode) VALUES ('$email', '$username', '$verifyCode')
        ";
        if ($this->conn->connectTO()->query($sql) === TRUE) { /* Nothing */ }
    }

    private function updateVerifyCode($email, $newCode) {
        $updateCode = "UPDATE users SET VerificationCode='$newCode' WHERE Email='$email'";
        if ($this->conn->connectTO()->query($updateCode) === TRUE) { /* Nothing */ }
    }

    public function setTimesDatabase($username, $qtime, $rtime) {
        $sqlQuali = "UPDATE users SET QTime='$qtime' WHERE Username='$username'";
        $sqlRace = "UPDATE users SET RTime='$rtime' WHERE Username='$username'";
        if ($this->conn->connectTO()->query($sqlQuali) === TRUE) { /* Nothing */ }
        if ($this->conn->connectTO()->query($sqlRace) === TRUE) { /* Nothing */ }
    }

    public function verify($event, $email, $username = null) {
        $code = $this->VerificationCode();
        switch($event) {
            case 'register':
                $this->toDatabase($email, $username, $code);
                break;
            case 'login':
                $this->updateVerifyCode($email, $code);
                break;
        } 
        // $this->send($email, "Registration RetroRacer - ${username}", $this->emailContent($username, $code));
    }
}

?>