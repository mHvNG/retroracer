<?php

class ReadWrite {

    public function read($file) {
        if ($fileToRead = fopen($file, 'r')) { /* Nothing */ } else return null;
        $content = fread($fileToRead, filesize($file));
        fclose($fileToRead);
        return $content;
    }

    public function parse($dbase, $dbaseName) {
        $loginValues = [];
        $json = json_decode($dbase, true);
        foreach($json as $key => $value) {
            if ($key == 'databases') {
                foreach($value as $key => $value) { array_push($loginValues, $value); }
            } else array_push($loginValues, $value);
        }
        return $loginValues;
    }
}

?>