<?php

class LeaderParser {

    function __construct() {
        
    }

    public function getRealTime($mode, $stringTime) {
        $qualiTimes = [];
        $raceTimes = [];
        switch($mode) {
            case 'qualification':
                List($sec, $milli) = explode(':', $stringTime);
                array_push($qualiTimes, $sec, $milli);
                return $qualiTimes;
            case 'race':
                List($min, $sec) = explode(':', $stringTime);
                array_push($qualiTimes, $min, $sec);
                return $raceTimes;
        }
    }
}

?>