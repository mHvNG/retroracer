<?php

include 'connection.php';

class Database {

    private $conn;

    function __construct() {
        $this->conn = new Connection();
    }

    private function getResult($column) {
        $list = [];
        $sql = "SELECT ${column} FROM users";
        $result = $this->conn->connectTo()->query($sql);
        while($row = $result->fetch_assoc()) {
            foreach($row as $item) { array_push($list, $item); }
        }
        return $list;
    }

    public function exists($column, $value) {
        $list = $this->getResult($column);
        foreach($list as $item) {
            if ($item == $value) { return true; }
        }
        return false;
    }

    public function getVerifyCode($email) {
        $index = 0;
        $emails = $this->getResult('Email');
        $codes = $this->getResult('VerificationCode');
        foreach($emails as $item) {
            if ($item == $email) { return $codes[$index]; }
            $index++;
        }
        return null;
    }

    public function getUsername($email) {
        $index = 0;
        $emails = $this->getResult('Email');
        $usernames = $this->getResult('Username');
        foreach($emails as $item) {
            if ($item == $email) { return $usernames[$index]; }
            $index++;
        }
        return null;
    }

    public function getTimesDatabase($column) {
        $list = [];
        $sql = "SELECT ${column} FROM users";
        $result = $this->conn->connectTo()->query($sql);
        while($row = $result->fetch_assoc()) {
            foreach($row as $item) { array_push($list, $item); }
        }
        return $list;
    }

    public function getUsernameTime($mode, $time) {
        $username = '';
        $sql = "SELECT Username FROM users WHERE ${mode}='$time'";
        $result = $this->conn->connectTo()->query($sql);
        while($row = $result->fetch_assoc()) {
            foreach($row as $item) { $username = $item; }
        }
        return $username;
    }
}

?>