<?php 

include_once 'src/database.php';
include_once 'src/connection.php';
include_once 'src/leaderParser.php';
include_once 'src/IO.php';

class Leaderbord {

    private $dbase;
    private $parser;

    function __construct() {
        $this->dbase = new Database();
        $this->parser = new LeaderParser();
    }

    private function addTimes($mode, $values) {
        $timesQuali = [];
        foreach ($values as $time) {
            $username = $this->dbase->getUsernameTime('QTime', $time);
            $list = $this->parser->getRealTime($mode, $time);
            $timeNumber = $list[0]. $list[1];
            $timeNumber = (int)$timeNumber;
            array_push($timesQuali, $timeNumber);
        }
        return $timesQuali;
    }

    private function getUsername($mode, $values) {
        $infoQuali = [];
        foreach ($values as $time) {
            $username = $this->dbase->getUsernameTime('QTime', $time);
            $list = $this->parser->getRealTime($mode, $time);
            $timeNumber = $list[0]. $list[1];
            $timeNumber = (int)$timeNumber;
            array_push($infoQuali, array($username, $timeNumber));
        }
        return $infoQuali;
    }

    private function filter($mode, $values) {
        $times = $this->addTimes($mode, $values);
        $timesToCheck = $this->getUsername($mode, $values);
        asort($times);
        $i= 0;
        foreach($times as $time) {
            foreach($timesToCheck[$i] as $value) {
                echo $value. ' - '. $time. '<br>';
                if ($time == $value) {
                    echo $timesToCheck[$i][0]. ' - '. $time. '<br>';
                }
            }
            echo 'new loop'. '<br>';
            $i++;
        }
    }

    public function render() {
        $listOfTimes = [];
        foreach ($this->dbase->getTimesDatabase('QTime') as $val) {
            array_push($listOfTimes, $val);
        }
        $this->filter('qualification', $listOfTimes);
    }
}

$leaderbord = new Leaderbord();
$leaderbord->render();

?>