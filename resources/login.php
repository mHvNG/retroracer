<?php

include_once 'src/datahandler.php';
include_once 'src/generator.php';
include_once 'src/database.php';
include_once 'src/IO.php';

class Login {

    private $handler;
    private $dbase;
    private $gen;
    private $io;

    function __construct() {
        $this->handler = new Datahandler();
        $this->dbase = new Database();
        $this->gen = new Generate();
        $this->io = new ReadWrite();
    }

    public function existence() {
        if (isset($_POST['email'])) {
            if ($this->dbase->exists('Email', $_POST['email'])) {
                $data = [
                    'email',
                    $_POST['email'],
                    'username',
                    $this->dbase->getUsername($_POST['email'])
                ];
                $this->gen->verify('login', $_POST['email']); 
                foreach(range(0, 3) as $index) { 
                    if ($index % 2 == 0) { $this->handler->upload($data[$index], $data[$index +1]); }
                }
                $this->handler->upload('email', $_POST['email']);
                header('Location: /resources/verify.php'); 
            }
            else {
                $alertMessage = 'Email is unknown! Try again or register your account';
                echo "<script type='text/javascript'>
                    alert('$alertMessage');
                </script>";
            }
        }
    }

    public function render() {
        echo $this->io->read('templates/login-content.html');
    }
}

$loginPage = new Login();

/* --> */ $loginPage->existence();
/* --> */ $loginPage->render();

?>