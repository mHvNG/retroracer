<?php

include_once 'src/datahandler.php';
include_once 'src/database.php';
include_once 'src/IO.php';

class RegisterPage {

    private $handler;
    private $dbase;
    private $io;

    function __construct() {
        $this->handler = new Datahandler();
        $this->dbase = new Database();
        $this->io = new ReadWrite();
    }

    public function isVerified() {
        if (isset($_POST['code'])) {
            if ($this->dbase->getVerifyCode($this->handler->retrieve('email')) === $_POST['code']) {
                /* Unset the unnecessary cookie / post data */ 
                if (isset($_COOKIE['email'])) { unset($_COOKIE['email']); setcookie('email', '', -1); }
                if (isset($_POST)) { unset($_POST); }
                header('Location: /resources/racer.php'); 
            }
        }
    }

    public function render() {
        echo $this->io->read('templates/verify-content.html');
    }
}

$registerPage = new RegisterPage();
/* --> */ $registerPage->isVerified();
/* --> */ $registerPage->render();

?>